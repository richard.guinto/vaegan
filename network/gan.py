from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Flatten, Reshape, Input
from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.layers import LeakyReLU, Dropout
from keras.layers import BatchNormalization
from keras.optimizers import RMSprop

class GAN(object):
    def __init__(self, img_rows=28, img_cols=28, channels=1):

        self.img_rows = img_rows
        self.img_cols = img_cols
        self.channels = channels
        self.img_shape = (img_rows, img_cols, channels)
        
        self.optimizer_disc = RMSprop(lr=0.0002, decay=6e-8)
        self.optimizer_gen = RMSprop(lr=0.0001, decay=3e-8)
        self.optimizer_adv = RMSprop(lr=0.0001, decay=3e-8)
        
        self.disc = self.discriminator_model()
        self.disc.compile(loss='binary_crossentropy', optimizer=self.optimizer_disc,\
            metrics=['accuracy'])
        
        self.gen = self.generator_model()
        self.gen.compile(loss='binary_crossentropy', optimizer=self.optimizer_gen,\
            metrics=['accuracy'])
        
        # The generator takes noise as input and generated imgs
        z = Input(shape=(100,))
        img = self.gen(z)
        
        valid = self.disc(img)

        self.adv = Model(z, valid)
        self.adv.compile(loss='binary_crossentropy', optimizer=self.optimizer_adv,\
            metrics=['accuracy'])


    def discriminator_model(self):
        depth = 64
        dropout = 0.4
        # In: 28 x 28 x 1, depth = 1
        # Out: 14 x 14 x 1, depth=64
        model = Sequential()
        model.add(Conv2D(depth*1, 5, strides=2, input_shape=self.img_shape,\
            padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(dropout))

        model.add(Conv2D(depth*2, 5, strides=2, padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(dropout))

        model.add(Conv2D(depth*4, 5, strides=2, padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(dropout))

        model.add(Conv2D(depth*8, 5, strides=1, padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(dropout))

        # Out: 1-dim probability
        model.add(Flatten())
        model.add(Dense(1))
        model.add(Activation('sigmoid'))
        model.summary()
        
        return model

    def generator_model(self):

        dropout = 0.4
        depth = 64+64+64+64
        dim = 7
        model = Sequential()
        # In: 100
        # Out: dim x dim x depth
        model.add(Dense(dim*dim*depth, input_dim=100))
        model.add(BatchNormalization(momentum=0.9))
        model.add(Activation('relu'))
        model.add(Reshape((dim, dim, depth)))
        model.add(Dropout(dropout))

        # In: dim x dim x depth
        # Out: 2*dim x 2*dim x depth/2
        model.add(UpSampling2D())
        model.add(Conv2DTranspose(int(depth/2), 5, padding='same'))
        model.add(BatchNormalization(momentum=0.9))
        model.add(Activation('relu'))

        model.add(UpSampling2D())
        model.add(Conv2DTranspose(int(depth/4), 5, padding='same'))
        model.add(BatchNormalization(momentum=0.9))
        model.add(Activation('relu'))

        model.add(Conv2DTranspose(int(depth/8), 5, padding='same'))
        model.add(BatchNormalization(momentum=0.9))
        model.add(Activation('relu'))

        # Out: 28 x 28 x 1 grayscale image [0.0,1.0] per pix
        model.add(Conv2DTranspose(1, 5, padding='same'))
        model.add(Activation('sigmoid'))
        model.summary()
        
        return model
        
