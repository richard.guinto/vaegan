import numpy as np # linear algebra

import keras
#from keras.applications.mobilenet import MobileNet
from keras.applications.xception import Xception
from keras.models import Model,Sequential, load_model
from keras.layers import Dropout, Flatten, Input, AveragePooling2D, merge, Activation
from keras.layers import Concatenate, multiply, Add,Flatten,ZeroPadding1D,Reshape
from keras.layers import Input, Dense, Conv2D, Conv2DTranspose, Lambda, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU

from keras.callbacks import Callback, ModelCheckpoint, CSVLogger, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop, Adam
from keras.utils import plot_model

from keras.datasets import mnist

from keras.losses import mse, binary_crossentropy
from keras import backend as K
from keras.layers import Deconv2D

import matplotlib.pyplot as plt
import os
from tqdm import tqdm
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import cv2

from keras import optimizers

import random
from random import shuffle

import math

img_size = 64
channels = 3
batch_size = 64
# epochs = 30000
epochs = 300000
sample_interval=200

learning_rate = .0003
beta1 = .5
z_dim = 128
gf_dim=64
df_dim=64
rows = [64]
cols = [64]
recon_vs_gan = 1e-6

predict_only = True
num_pred = 10
default_datapath = '/home/rangel/Documents/Datasets/CelebA/img_align_celeba/'


checkpt_path = '../Checkpoints'
load_old = True
old_wts_path_gan = 'vaegan_gan-208200.hdf5'
old_wts_path_disc = 'vaegan_discriminator-208200.hdf5'
old_wts_path_enc = 'vaegan_encoder-208200.hdf5'


def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


def build_encoder(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels))

	x = Conv2D(64, (5,5), strides=(2,2), padding='same', name='e_conv1')(i)
	x = BatchNormalization(name='e_bn1')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='e_conv2')(x)
	x = BatchNormalization(name='e_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='e_conv3')(x)
	x = BatchNormalization(name='e_bn3')(x)
	#x = Activation('relu')(x)
	# x = LeakyReLU(.2)(x)
	# x = Conv2D(512, (5,5), strides=(2,2), padding='same', use_bias=False, name='e_conv4')(x)
	# x = BatchNormalization()(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Flatten()(x)


	mean = Dense(z_dim, name="e_dense1")(x)
	logsigma = Dense(z_dim, name="e_dense2")(x)

	mean = BatchNormalization(name='e_mean_bn1')(mean)
	logsigma = BatchNormalization(name='e_logisgma_bn1')(logsigma)

	mean = LeakyReLU(.2)(mean)
	logsigma = LeakyReLU(.2)(logsigma)
	
	z = Lambda(sampling, output_shape=(z_dim,), name='z')([mean, logsigma])

	enc = Model([i], [mean,logsigma, z], name='encoder')
	enc.summary()
	return enc

def build_generator():
	i = Input(shape=(z_dim,))	#generator with input noise
	x = Dense(8*8*256)(i)
	# x = Dense(4*4*256)(i)		
	# x = Reshape((4,4,256))(x)
	x = BatchNormalization(name='g_bn0')(x)
	x = LeakyReLU(.2)(x)
	x = Reshape((8,8,256))(x)
	
	#not sure if Conv2D transpose is equivalent to backward conv
	x = Conv2DTranspose(256, (5,5), strides=(2,2), padding='same', name='g_conv1')(x)
	x = BatchNormalization(name='g_bn1')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(128, (5,5), strides=(2,2), padding='same', name='g_conv2')(x)
	x = BatchNormalization(name='g_bn2')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(32, (5,5), strides=(2,2), padding='same', name='g_conv3')(x)
	x = BatchNormalization(name='g_bn3')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(channels, (5,5), strides=(1,1), padding='same')(x)
	out = Activation('tanh')(x)

	gen = Model([i], [out], name='generator')
	gen.summary()

	return gen
	

# def build_decoder(i):
# 	x = Dense(8*8*256)(i)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Reshape((8,8,256))(x)
# 	#not sure if Conv2D transpose is equivalent to backward conv
# 	x = Conv2DTranspose(256, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv1')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2DTranspose(128, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv2')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2DTranspose(32, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv3')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2D(3, (5,5), strides=(1,1), padding='same', use_bias=False)(x)
# 	out = Activation('tanh')(x)

# 	dec = Model(i, out)
# 	dec.summary()

# 	return dec

def build_discriminator(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels), name="d_input")

	x = Conv2D(32, (5,5), strides=(1,1), padding='same', name='d_conv1')(i)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='d_conv2')(x)
	x = BatchNormalization(name='d_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv3')(x)
	x = BatchNormalization(name='d_bn3')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv4')(x)


	dec = BatchNormalization(name='d_bn4')(x)
	#x = Activation('relu')(x)
	dec = LeakyReLU(.2)(dec)
	dec = Flatten()(dec)
	dec = Dense(512)(dec)
	dec = BatchNormalization(name='d_bn5')(dec)
	
	#x = Activation('relu')(x)
	dec = LeakyReLU(.2)(dec)
	# dec = Flatten()(dec)
	dec = Dense(1)(dec)
	out = Activation('sigmoid')(dec)

	disc1 = Model([i], [out], name='discriminator1')
	disc2 = Model([i], [x], name='discriminator2')
	disc1.summary()
	disc2.summary()

	return disc1, disc2





def dataGen(img_size = 64, channels = 3, batch_size = 64, dataset_path=default_datapath):
	list = os.listdir(dataset_path)
	num_files = len(list)
	list.sort()

	while True:
		random.shuffle(list)
		img_list = list[:]

		while img_list:
			imgs = np.zeros((batch_size, img_size, img_size, 3), dtype=np.float32)
			for i in range(batch_size):
				filename = dataset_path + img_list.pop(0)
				img = cv2.imread(filename)

				if img.shape!=(64,64,3):
					imgs[i,:,:,:] = cv2.resize(img, (img_size, img_size))/255
					imgs[i,:,:,:] = imgs[i,:,:,:]*2 -1
				if len(img_list)==0:
					random.shuffle(list)
					img_list = list[:]

			yield imgs, None




optimizer = optimizers.rmsprop(0.0003)



##################
'''BUILD MODELS'''
##################


# Adversarial ground truths
valid = np.ones((batch_size, 1))
fake = np.zeros((batch_size, 1))




#optimizer = Adam(0.0002, 0.5)
encoder = build_encoder()
generator = build_generator()
discriminator, l_disc= build_discriminator()


inputs = Input(shape=(img_size, img_size, channels))
z_mean, z_log_var, z = encoder(inputs)
out1 = generator(z)
iden1 = discriminator(out1)
model1 = l_disc(out1)			#predicted
iden2= discriminator(inputs)
model2 = l_disc(inputs)			#target
vaegan_encoder = Model(inputs, [out1, model1, model2], name='vaegan_encoder')



#like loss (compare model1 and model2) - assume variance = 1
# x_prec = K.exp(-1.)
x_prec = 1.
x_diff = model1 - model2
x_power = (x_diff * x_diff) * x_prec * -0.5
# loss = (1. + math.log(2 * math.pi)) / 2 - x_power
loss = (0. + math.log(2 * math.pi)) / 2 - x_power
like_loss = K.sum(loss)
# like_loss = K.mean(loss)

# sigma = 1.
# c = -0.5*np.log(2*np.pi)
# multiplier = 1.0/(2.0*sigma**2)
# tmp = model1 - model2
# tmp **=2
# tmp *= (-1.*multiplier)
# tmp += c
# like_loss = K.sum(tmp)



#vae loss
original_dim = 64*64*3
reconstruction_loss = mse(K.flatten(inputs), K.flatten(out1))
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
# vae_loss = K.mean(reconstruction_loss + kl_loss)
vae_loss = K.mean(kl_loss)

encoder_loss = K.mean(kl_loss + like_loss)

# vaegan_encoder.add_loss(vae_loss)
# vaegan_encoder.add_loss(like_loss)
vaegan_encoder.add_loss(encoder_loss)
generator.trainable = False
discriminator.trainable = False
l_disc.trainable = False
vaegan_encoder.compile(optimizer=optimizer)
print('\n\nvaegan encoder:')
vaegan_encoder.summary()
plot_model(vaegan_encoder, to_file='vaegan_encoder.png', show_shapes=True)
generator.trainable = True
discriminator.trainable = True
l_disc.trainable = True
encoder.trainable = False



n_disc_trainable = len(discriminator.trainable_weights)
print('n_disc_trainable: ', n_disc_trainable)

z_gen = Input(shape=(z_dim,))
img = generator(z_gen)
identifier = discriminator(img)
model = l_disc(img)

# d_loss_fake1 = binary_crossentropy(identifier, K.zeros_like(identifier))	#output from noise
# d_loss_fake2 = binary_crossentropy(iden1, K.zeros_like(iden1))			#output from vae
# d_loss_real = binary_crossentropy(iden2, K.ones_like(iden2))
# d_loss = -1.*K.mean(d_loss_fake1 + d_loss_fake2 + d_loss_real)

vaegan_discriminator = discriminator
# vaegan_discriminator = Model(discriminator.get_layer("d_input"), identifier, name='vaegan_discriminator')
# vaegan_discriminator.add_loss(d_loss)
# vaegan_discriminator.add_loss(like_loss*recon_vs_gan)
vaegan_discriminator.compile(loss='binary_crossentropy',optimizer=optimizer, metrics=['accuracy'])
print('\n\nvaegan discriminator:')
vaegan_discriminator.summary()
plot_model(vaegan_discriminator, to_file='vaegan_discriminator.png', show_shapes=True)
n_gen_trainable = len(generator.trainable_weights)
print('n_gen_trainable: ', n_gen_trainable)
discriminator.trainable = False




# def gan_loss(yTrue, yPred):
# 	loss = binary_crossentropy()

# g_loss0 = binary_crossentropy(K.ones_like(iden2), iden2)
g_loss1 = binary_crossentropy(K.zeros_like(iden2), iden1)			#output from vae
g_loss2 = binary_crossentropy(K.zeros_like(iden2), identifier)	#output from noise
g_loss = K.mean(g_loss1 + g_loss2)

vaegan_gan = Model([inputs, z_gen], [identifier,iden1, iden2], name='vaegan_gan')
vaegan_gan.add_loss(-1.0*g_loss)
vaegan_gan.add_loss(recon_vs_gan*like_loss)
vaegan_gan.compile(optimizer=optimizer)
print('\n\nvaegan gan:')
vaegan_gan.summary()
plot_model(vaegan_gan, to_file='vaegan_gan.png', show_shapes=True)

print("\n\nafter compiling vaegan_gan...")
discriminator.trainable = True
encoder.trainable = True
n_disc_trainable = len(discriminator.trainable_weights)
print('n_disc_trainable: ', n_disc_trainable)
print('len(discriminator._collected_trainable_weights): ', len(discriminator._collected_trainable_weights))
n_gen_trainable = len(generator.trainable_weights)
print('n_gen_trainable: ', n_gen_trainable)
print('len(vaegan_gan._collected_trainable_weights): ', len(vaegan_gan._collected_trainable_weights))





##############
'''TRAINING'''
##############



datagenerator = dataGen(batch_size=batch_size, img_size=img_size, channels=channels)

if load_old==True:
	vaegan_gan.load_weights(old_wts_path_gan)
	vaegan_discriminator.load_weights(old_wts_path_disc)
	vaegan_encoder.load_weights(old_wts_path_enc)

if predict_only==False:
	for epoch in range(epochs):

	    # ---------------------
	    #  Train Encoder
	    # ---------------------

		imgs,_ = next(datagenerator)

		v_loss = vaegan_encoder.train_on_batch(imgs, None)


		# ---------------------
	    #  Train Discriminator
	    # ---------------------
		imgs,_ = next(datagenerator)
		imgs2,_ = next(datagenerator)
		vae_imgs, model1, model2 = vaegan_encoder.predict(imgs2)
		noise = np.random.normal(0, 1, (batch_size, z_dim))
		# noise = np.random.uniform(-2., 2., (batch_size, z_dim))

	    # Generate a batch of new images
		gen_imgs = generator.predict(noise)

		d1 = vaegan_discriminator.train_on_batch(imgs, valid)
		d2 = vaegan_discriminator.train_on_batch(gen_imgs, fake)
		d3 = vaegan_discriminator.train_on_batch(vae_imgs, fake)
		d_loss = np.add(d1,d2)
		d_loss = np.add(d_loss, d3)/3

	    # ------------------------
	    #  Train Generator/Decoder
	    # ------------------------

		
		# _,_,latent = encoder.predict
		

	    # Train the generator (to have the discriminator label samples as valid)
		# g_loss = vaegan_gan.train_on_batch(noise, valid)
		imgs,_ = next(datagenerator)
		a,b,latent = encoder.predict(imgs)
		# print('z mean: ', np.mean(a))
		# print('z var: ', np.mean(b))
		# noise = np.random.uniform(-2., 10., (batch_size, z_dim))
		noise = np.random.normal(0, 1, (batch_size, z_dim))
		# noise = np.random.normal(np.mean(a), math.exp(np.mean(b)), (batch_size, z_dim))
		# print('latent max: ', np.amax(latent))
		# print('latent min: ', np.amin(latent))
		# print('noise: ', noise)
		# g1 = vaegan_gan.train_on_batch(noise, None)
		# g2 = vaegan_gan.train_on_batch(imgs, None)
		# g_loss = 0.5*np.add(g1+g2)
		g_loss = vaegan_gan.train_on_batch([imgs, noise], None)


	    # Plot the progress
		print ("step: %d [V loss: %f] [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, v_loss, d_loss[0], 100*d_loss[1], g_loss))
		# print("steps: %d" % epoch)


	    # ------------------------
	    #  Show Sample Predictions
	    # ------------------------


	    # If at save interval => save generated image samples
		if epoch % sample_interval == 0:
		# if epoch % 10 == 0:
			r, c = 5, 5
			# noise = np.random.normal(np.mean(a), math.exp(np.mean(b)),(10, z_dim))
			# noise = np.append(noise, np.random.normal(0, 1,((r*c)-10, z_dim)), axis=0)
			# noise = np.random.normal(0, 1, (r * c, z_dim))
			noise = np.random.uniform(-2., 10., (r * c, z_dim))
			gen_imgs = generator.predict(noise)

			# Rescale images 0 - 1
			gen_imgs = 0.5 * gen_imgs + 0.5

			fig, axs = plt.subplots(r, c)
			cnt = 0
			for i in range(r):
			    for j in range(c):
			        b,g,r = cv2.split(gen_imgs[cnt, :,:,:])
			        rgb_img = cv2.merge([r,g,b])
			        axs[i,j].imshow(rgb_img)
			        axs[i,j].axis('off')
			        cnt += 1
			fig.savefig("images_vaegan/gan%d.png" % (64000+epoch))

			imgs,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
			vae_imgs, model1, model2 = vaegan_encoder.predict(imgs)
			# Rescale images 0 - 1
			vae_imgs = 0.5 * vae_imgs + 0.5

			r, c = 5, 10
			fig, axs = plt.subplots(r, c)
			cnt = 0
			for i in range(r):
			    for j in range(c//2):
			        b,g,r = cv2.split(vae_imgs[cnt, :,:,:])
			        rgb_img = cv2.merge([r,g,b])
			        axs[i,j].imshow(rgb_img)
			        axs[i,j].axis('off')
			        b,g,r = cv2.split(imgs[cnt, :,:,:])
			        rgb_img = cv2.merge([r,g,b])
			        axs[i,j+5].imshow(rgb_img)
			        axs[i,j+5].axis('off')
			        cnt += 1
			fig.savefig("images_vaegan/vae%d.png" % (64000+epoch))

			vaegan_gan.save_weights('images_vaegan/vaegan_gan-%d.hdf5' % (64000+epoch))
			vaegan_discriminator.save_weights('images_vaegan/vaegan_discriminator-%d.hdf5' % (64000+epoch))
			vaegan_encoder.save_weights('images_vaegan/vaegan_encoder-%d.hdf5' % (64000+epoch))
			plt.close()

else:

	for k in range(num_pred):
		vaegan_gan.load_weights(old_wts_path_gan)
		vaegan_discriminator.load_weights(old_wts_path_disc)
		vaegan_encoder.load_weights(old_wts_path_enc)

		# imgs,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
		r, c = 5, 5
		# noise = np.random.normal(np.mean(a), math.exp(np.mean(b)),(10, z_dim))
		# noise = np.append(noise, np.random.normal(0, 1,((r*c)-10, z_dim)), axis=0)
		# noise = np.random.normal(0, 1, (r * c, z_dim))
		noise = np.random.uniform(-0., 10., (r * c, z_dim))
		gen_imgs = generator.predict(noise)

		# Rescale images 0 - 1
		gen_imgs = 0.5 * gen_imgs + 0.5

		fig, axs = plt.subplots(r, c)
		cnt = 0
		for i in range(r):
		    for j in range(c):
		        b,g,r = cv2.split(gen_imgs[cnt, :,:,:])
		        rgb_img = cv2.merge([r,g,b])
		        axs[i,j].imshow(rgb_img)
		        axs[i,j].axis('off')
		        cnt += 1
		fig.savefig("gan_vaegan%d.png" % k)

		imgs,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
		vae_imgs, model1, model2 = vaegan_encoder.predict(imgs)
		# Rescale images 0 - 1
		vae_imgs = 0.5 * vae_imgs + 0.5
		imgs = 0.5*imgs + 0.5

		r, c = 5, 10
		fig, axs = plt.subplots(r, c)
		cnt = 0
		for i in range(r):
		    for j in range(c//2):
		        b,g,r = cv2.split(vae_imgs[cnt, :,:,:])
		        rgb_img = cv2.merge([r,g,b])
		        axs[i,j].imshow(rgb_img)
		        axs[i,j].axis('off')
		        b,g,r = cv2.split(imgs[cnt, :,:,:])
		        rgb_img = cv2.merge([r,g,b])
		        axs[i,j+5].imshow(rgb_img)
		        axs[i,j+5].axis('off')
		        cnt += 1
		fig.savefig("vae_vaegan%d.png" % k)

		plt.close()
