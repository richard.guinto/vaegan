import numpy as np # linear algebra

from keras.models import Model
from keras.layers import Dropout, Flatten, Input, AveragePooling2D, merge, Activation
from keras.layers import Concatenate, multiply, Add,Flatten,ZeroPadding1D,Reshape
from keras.layers import Input, Dense, Conv2D, Conv2DTranspose, Lambda, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU

from keras.callbacks import Callback, ModelCheckpoint, CSVLogger, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop, Adam
from keras.utils import plot_model
from keras.losses import mse, binary_crossentropy
from keras import backend as K

import matplotlib.pyplot as plt
import os
import cv2

from keras import optimizers

import random


img_size = 64
channels = 3
batch_size = 64
# epochs = 30000
epochs = 10
sample_interval=200

learning_rate = .0003
beta1 = .5
z_dim = 128
gf_dim=64
df_dim=64
rows = [8]
cols = [8]
recon_vs_gan = 1e-6

predict_only = True
checkpt_path = 'checkpoints'


def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


def build_encoder(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels))

	x = Conv2D(64, (5,5), strides=(2,2), padding='same', name='e_conv1')(i)
	x = BatchNormalization(name='e_bn1')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='e_conv2')(x)
	x = BatchNormalization(name='e_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='e_conv3')(x)
	x = BatchNormalization(name='e_bn3')(x)
	#x = Activation('relu')(x)
	# x = LeakyReLU(.2)(x)
	# x = Conv2D(512, (5,5), strides=(2,2), padding='same', use_bias=False, name='e_conv4')(x)
	# x = BatchNormalization()(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Flatten()(x)


	mean = Dense(z_dim, name="e_dense1")(x)
	logsigma = Dense(z_dim, name="e_dense2")(x)

	mean = BatchNormalization(name='e_mean_bn1')(mean)
	logsigma = BatchNormalization(name='e_logisgma_bn1')(logsigma)

	mean = LeakyReLU(.2)(mean)
	logsigma = LeakyReLU(.2)(logsigma)
	
	z = Lambda(sampling, output_shape=(z_dim,), name='z')([mean, logsigma])

	enc = Model([i], [mean,logsigma, z])
	enc.summary()
	return enc

def build_generator():
	i = Input(shape=(z_dim,))	#generator with input noise
	x = Dense(8*8*256)(i)
	# x = Dense(4*4*256)(i)		
	# x = Reshape((4,4,256))(x)
	x = BatchNormalization(name='g_bn0')(x)
	x = LeakyReLU(.2)(x)
	x = Reshape((8,8,256))(x)
	
	#not sure if Conv2D transpose is equivalent to backward conv
	x = Conv2DTranspose(256, (5,5), strides=(2,2), padding='same', name='g_conv1')(x)
	x = BatchNormalization(name='g_bn1')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(128, (5,5), strides=(2,2), padding='same', name='g_conv2')(x)
	x = BatchNormalization(name='g_bn2')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(32, (5,5), strides=(2,2), padding='same', name='g_conv3')(x)
	x = BatchNormalization(name='g_bn3')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(channels, (5,5), strides=(1,1), padding='same')(x)
	out = Activation('tanh')(x)

	gen = Model([i], [out])
	gen.summary()

	return gen
	


def build_discriminator(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels))

	x = Conv2D(32, (5,5), strides=(2,2), padding='same', name='d_conv1')(i)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='d_conv2')(x)
	x = BatchNormalization(name='d_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv3')(x)
	x = BatchNormalization(name='d_bn3')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv4')(x)
	x = BatchNormalization(name='d_bn4')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Dense(512)(x)
	x = BatchNormalization(name='d_bn5')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Flatten()(x)
	x = Dense(1)(x)
	out = Activation('sigmoid')(x)

	disc = Model([i], [out])
	disc.summary()

	return disc


#dataset_path='/hdd/Datasets/CelebA/Img/img_align_celeba_png.7z/img_align_celeba_png/'
#dataset_path='/hdd/Datasets/CelebA/Img/img_align_celeba/'
#dataset_path='/home/rangeldaroya/Documents/img_align_celeba/'
dataset_path = 'dataset/input'
def dataGen(img_size = 64, channels = 3, batch_size = 64, dataset_path='/home/rangel/Documents/Datasets/CelebA/img_align_celeba/'):
	
	list = os.listdir(dataset_path)
	list.sort()

	while True:
		random.shuffle(list)
		img_list = list[:]

		while img_list:
			imgs = np.zeros((batch_size, img_size, img_size, 3), dtype=np.float32)

			for i in range(batch_size):
				filename = dataset_path + '/' + img_list.pop(0)
				img = cv2.imread(filename)

				if img.shape!=(64,64,3):
					imgs[i,:,:,:] = cv2.resize(img, (img_size, img_size))/255
					imgs[i,:,:,:] = imgs[i,:,:,:]*2 - 1
				if len(img_list)==0:
					random.shuffle(list)
					img_list = list[:]
			yield imgs, None


def plot_results(models,
                 data,
                 batch_size=128,
                 model_name="vae_mnist"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """

    encoder, decoder = models
    x_test, y_test = data
    filename = os.path.join(model_name, "digits_over_latent.png")
    # display a 30x30 2D manifold of digits
    n = 30
    digit_size = img_size
    figure = np.zeros((digit_size * n, digit_size * n, channels))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-4, 4, n)
    grid_y = np.linspace(-4, 4, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            x_decoded = decoder.predict(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[i * digit_size: (i + 1) * digit_size,
                   j * digit_size: (j + 1) * digit_size] = digit

    plt.figure(figsize=(8, 8))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range + 1
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.imshow(figure, cmap='Greys_r')
    plt.savefig(filename)
    plt.show()



encoder = build_encoder(img_size=img_size, channels=channels)
decoder = build_generator()


inputs = Input(shape=(img_size, img_size, channels))
z_mean, z_log_var, z = encoder(inputs)
outputs = decoder(z)
print('output shape: ', outputs.get_shape())
vae = Model(inputs,outputs, name='vae')


models = (encoder, decoder)
#x_test = dataGenerator('test', batch_size=batch_size, img_size=img_size, channels=channels)
# data = (x_test, y_test)

original_dim = 64*64*3
# reconstruction_loss = binary_crossentropy(inputs, outputs)
reconstruction_loss = mse(K.flatten(inputs), K.flatten(outputs))
# print()
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
vae_loss = K.mean(reconstruction_loss + kl_loss)


#for likelihood loss, see: https://github.com/chainer/chainer/blob/master/chainer/functions/loss/vae.py
#and see: https://docs.chainer.org/en/stable/reference/generated/chainer.functions.gaussian_nll.html
# x_prec = K.exp(-z_log_var)
# x_diff = z - z_mean
# x_power = (x_diff * x_diff) * x_prec * -0.5
# loss = (z_log_var + math.log(2 * math.pi)) / 2 - x_power
# like_loss = K.sum(loss)
# print('vae_loss: ', vae_loss.get_shape())



vae.add_loss(vae_loss)
optimizer = optimizers.rmsprop(0.0003)

# vae.add_loss(like_loss)                                                                           
vae.compile(optimizer=optimizer)
vae.summary()


# vae.fit(x_train, epochs=epochs, batch_size=batch_size, validation_data=(x_test,None))
# a,b = next(dataGenerator('train'))
# print('a: ', a.shape)
if predict_only==False:
	checkpointer = ModelCheckpoint(filepath=checkpt_path + '/model-{epoch:02d}.hdf5', verbose=1)
	#vae.fit_generator(dataGen(batch_size=batch_size, img_size=img_size, channels=channels), len(train_idx)//batch_size, epochs, callbacks=[checkpointer])
	vae.fit_generator(dataGen(batch_size=batch_size, img_size=img_size, channels=channels), 200000//batch_size, epochs, callbacks=[checkpointer])

	vae.save_weights('vae_finalweights.hdf5')

	# data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
	# plot_results(models, data, batch_size=batch_size,model_name="vae")

	data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))

	out = vae.predict(data)
	out = (out+1.)*127.5
	data = (data+1.)*127.5
	for i in range(data.shape[0]):
		cv2.imshow('output', out[i].astype('uint8'))
		cv2.imshow('data', data[i].astype('uint8'))
		cv2.waitKey(0)
		cv2.destroyAllWindows()
	# vae.predict()


else:
	vae.load_weights('vae_finalweights.hdf5')
#	plot_results(models, (x_test,None), batch_size=batch_size,model_name="vae")
	data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))

	out = vae.predict(data)
	# out = (out+1.)*127.5
	# data = (data+1.)*127.5
	# for i in range(data.shape[0]):
	# 	cv2.imshow('output', out[i].astype('uint8'))
	# 	cv2.imshow('data', data[i].astype('uint8'))
	# 	cv2.waitKey(0)
	# 	cv2.destroyAllWindows()

	r, c = 5, 10
	fig, axs = plt.subplots(r, c)
	cnt = 0
	for i in range(r):
	    for j in range(c//2):
	        b,g,r = cv2.split(out[cnt, :,:,:])
	        rgb_img = cv2.merge([r,g,b])
	        axs[i,j].imshow(rgb_img)
	        axs[i,j].axis('off')
	        b,g,r = cv2.split(data[cnt, :,:,:])
	        rgb_img = cv2.merge([r,g,b])
	        axs[i,j+5].imshow(rgb_img)
	        axs[i,j+5].axis('off')
	        cnt += 1
	fig.savefig("vae_images.png")

