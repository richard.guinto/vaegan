import numpy as np # linear algebra

import keras
#from keras.applications.mobilenet import MobileNet
from keras.applications.xception import Xception
from keras.models import Model,Sequential, load_model
from keras.layers import Dropout, Flatten, Input, AveragePooling2D, merge, Activation
from keras.layers import Concatenate, multiply, Add,Flatten,ZeroPadding1D,Reshape
from keras.layers import Input, Dense, Conv2D, Conv2DTranspose, Lambda, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU

from keras.callbacks import Callback, ModelCheckpoint, CSVLogger, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop, Adam
from keras.utils import plot_model

from keras.datasets import mnist

from keras.losses import mse, binary_crossentropy
from keras import backend as K
from keras.layers import Deconv2D

import matplotlib.pyplot as plt
import os
from tqdm import tqdm
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import cv2

from keras import optimizers

import random
from random import shuffle

import math

img_size = 64
channels = 3
batch_size = 64
# epochs = 30000
epochs = 300000
sample_interval=200

learning_rate = .0003
beta1 = .5
z_dim = 128
gf_dim=64
df_dim=64
rows = [64]
cols = [64]
recon_vs_gan = 1e-6

predict_only = False
checkpt_path = '../Checkpoints'

load_old = True
old_wts_path = 'images_old/model109200.hdf5'


def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


def build_encoder(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels))

	x = Conv2D(64, (5,5), strides=(2,2), padding='same', name='e_conv1')(i)
	x = BatchNormalization(name='e_bn1')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='e_conv2')(x)
	x = BatchNormalization(name='e_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='e_conv3')(x)
	x = BatchNormalization(name='e_bn3')(x)
	#x = Activation('relu')(x)
	# x = LeakyReLU(.2)(x)
	# x = Conv2D(512, (5,5), strides=(2,2), padding='same', use_bias=False, name='e_conv4')(x)
	# x = BatchNormalization()(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Flatten()(x)


	mean = Dense(z_dim, name="e_dense1")(x)
	logsigma = Dense(z_dim, name="e_dense2")(x)

	mean = BatchNormalization(name='e_mean_bn1')(mean)
	logsigma = BatchNormalization(name='e_logisgma_bn1')(logsigma)

	mean = LeakyReLU(.2)(mean)
	logsigma = LeakyReLU(.2)(logsigma)
	
	z = Lambda(sampling, output_shape=(z_dim,), name='z')([mean, logsigma])

	enc = Model([i], [mean,logsigma, z])
	enc.summary()
	return enc

def build_generator():
	i = Input(shape=(z_dim,))	#generator with input noise
	x = Dense(8*8*256)(i)
	# x = Dense(4*4*256)(i)		
	# x = Reshape((4,4,256))(x)
	x = BatchNormalization(name='g_bn0')(x)
	x = LeakyReLU(.2)(x)
	x = Reshape((8,8,256))(x)
	
	#not sure if Conv2D transpose is equivalent to backward conv
	x = Conv2DTranspose(256, (5,5), strides=(2,2), padding='same', name='g_conv1')(x)
	x = BatchNormalization(name='g_bn1')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(128, (5,5), strides=(2,2), padding='same', name='g_conv2')(x)
	x = BatchNormalization(name='g_bn2')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(32, (5,5), strides=(2,2), padding='same', name='g_conv3')(x)
	x = BatchNormalization(name='g_bn3')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2DTranspose(channels, (5,5), strides=(1,1), padding='same')(x)
	out = Activation('tanh')(x)

	gen = Model([i], [out])
	gen.summary()

	return gen
	

# def build_decoder(i):
# 	x = Dense(8*8*256)(i)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Reshape((8,8,256))(x)
# 	#not sure if Conv2D transpose is equivalent to backward conv
# 	x = Conv2DTranspose(256, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv1')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2DTranspose(128, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv2')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2DTranspose(32, (5,5), strides=(2,2), padding='same', use_bias=False, name='d_conv3')(x)
# 	x = BatchNormalization()(x)
# 	x = Activation('relu')(x)
# 	x = Conv2D(3, (5,5), strides=(1,1), padding='same', use_bias=False)(x)
# 	out = Activation('tanh')(x)

# 	dec = Model(i, out)
# 	dec.summary()

# 	return dec

def build_discriminator(img_size = 64, channels = 3):
	i = Input(shape=(img_size, img_size, channels))

	x = Conv2D(32, (5,5), strides=(2,2), padding='same', name='d_conv1')(i)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(128, (5,5), strides=(2,2), padding='same', name='d_conv2')(x)
	x = BatchNormalization(name='d_bn2')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv3')(x)
	x = BatchNormalization(name='d_bn3')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Conv2D(256, (5,5), strides=(2,2), padding='same', name='d_conv4')(x)
	x = BatchNormalization(name='d_bn4')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)

	x = Dense(512)(x)
	x = BatchNormalization(name='d_bn5')(x)
	#x = Activation('relu')(x)
	x = LeakyReLU(.2)(x)
	x = Flatten()(x)
	x = Dense(1)(x)
	out = Activation('sigmoid')(x)

	disc = Model([i], [out])
	disc.summary()

	return disc


def partitionImgs(datapath='/hdd/Datasets/CelebA/Eval/list_eval_partition.txt'):
	train_idx = np.array([])
	val_idx = np.array([])
	test_idx = np.array([])
	with open(datapath, 'r') as f:
		f.seek(0)
		for n, line in enumerate(f):
			# print('n: ', n)
			img_name, l = line.split()
			l = int(l)
			if l==0:		#training data
				train_idx = np.append(train_idx, [n])
			elif l==1:
				val_idx = np.append(val_idx, [n])
			elif l==2:
				test_idx = np.append(test_idx, [n])
			#print('img_name: ', img_name, 'l: ', int(l))
			# print('{:06}.png'.format(n+1)) #image name of corresponding index (NOTE THE n+1)


	print('num train: ', len(train_idx))
	# print(train_idx)
	print('num val: ', len(val_idx))
	# print(val_idx)
	print('num test: ', len(test_idx))
	# print(test_idx)
	num_data = len(test_idx) + len(train_idx) + len(val_idx)
	print('total data: ', num_data)
	return train_idx, val_idx, test_idx, num_data


def getAttributes(num_data, datapath='/hdd/Datasets/CelebA/Anno/list_attr_celeba.txt'):
	word_attributes = np.array([])
	

	with open(datapath, 'r') as f:
		f.seek(0)
		for n, line in enumerate(f):
			if n==1:
				word_attributes = np.append(word_attributes,line.split())
				num_attributes = np.zeros((num_data, len(word_attributes)))
			elif n>1:
				temp = line.split()[1:len(word_attributes)+1]
				temp = np.array(temp).astype('int')
				num_attributes[n-2,:] = temp

	print('word_attributes: ', len(word_attributes), '\n')
	print(word_attributes)
	print('num_attributes: ', num_attributes.shape, '\n')
	print(num_attributes)

	return word_attributes, num_attributes


# def random_sampler(datapath, )


def refreshIdx(split):
	global train_idx, val_idx, test_idx
	global copy_train_idx, copy_val_idx, copy_test_idx

	if split=='train':
		copy_train_idx = np.copy(train_idx)
	elif split=='valid':
		copy_val_idx = np.copy(val_idx)
	elif split=='test':
		copy_test_idx = np.copy(test_idx)
	else:
		print('INVALID SPLIT IN REFRESH IDX')


def dataGen(img_size = 64, channels = 3, batch_size = 64, dataset_path='/home/rangel/Documents/Datasets/CelebA/img_align_celeba/'):
	list = os.listdir(dataset_path)
	num_files = len(list)
	list.sort()

	while True:
		random.shuffle(list)
		img_list = list[:]

		while img_list:
			imgs = np.zeros((batch_size, img_size, img_size, 3), dtype=np.float32)
			for i in range(batch_size):
				filename = dataset_path + img_list.pop(0)
				img = cv2.imread(filename)

				if img.shape!=(64,64,3):
					imgs[i,:,:,:] = cv2.resize(img, (img_size, img_size))/255
					imgs[i,:,:,:] = imgs[i,:,:,:]*2 -1
				if len(img_list)==0:
					random.shuffle(list)
					img_list = list[:]

			yield imgs, None


#dataset_path='/hdd/Datasets/CelebA/Img/img_align_celeba_png.7z/img_align_celeba_png/'
#dataset_path='/hdd/Datasets/CelebA/Img/img_align_celeba/'
#dataset_path='/home/rangeldaroya/Documents/img_align_celeba/'
def BadDataGenerator(split, n_attr=40, img_size = 64, channels = 3, batch_size = 64, dataset_path='/home/rangeldaroya/Documents/img_align_celeba/'):
	imgs = np.zeros((batch_size, img_size, img_size, 3), dtype=np.float32)
	labels = np.zeros((batch_size, n_attr))
	idx = np.zeros((batch_size))
	global copy_train_idx, copy_val_idx, copy_test_idx
	# global num_attributes

	while True:
		if split == 'train':
			temp = copy_train_idx
		elif split == 'valid':
			temp = copy_val_idx
		elif split == 'test':
			temp = copy_test_idx
		else:
			print('\n\n\n INVALID SPLIT FOR LOADER \n\n\n')
			yield 0


		if len(temp) == 0:
			print('len(temp) == 0')
			refreshIdx(split)
			if split == 'train':
				temp = copy_train_idx
			elif split == 'valid':
				temp = copy_val_idx
			elif split == 'test':
				temp = copy_test_idx
			shuffle(temp)
			temp = list(temp)
			idx = random.sample(temp, batch_size)

		elif len(temp) < batch_size:
			shuffle(temp)
			temp = list(temp)
			# print('len(temp) < batch_size')
			r = len(temp)
			idx[0:r] = np.array(random.sample(temp, r))
			
			refreshIdx(split)
			if split == 'train':
				temp = copy_train_idx
			elif split == 'valid':
				temp = copy_val_idx
			elif split == 'test':
				temp = copy_test_idx

			shuffle(temp)
			temp = list(temp)
			# print('sample: ', idx)
			x = np.array(random.sample(temp, batch_size - r))
			for n,i in enumerate(x):
				idx[r+n] = i

		else: # len(temp) >= batch_size:
			shuffle(temp)
			temp = list(temp)
			# print('len(temp) >= batch_size')
			idx = random.sample(temp, batch_size)	#indices of sample images


		for i in range(batch_size):
			filepath = dataset_path + '{:06}.jpg'.format(int(idx[i]+1)) 	#image indices are from 0 to end-1 (image names start at 1)
			raw = cv2.imread(filepath)
			imgs[i,:,:,:] = cv2.resize(raw, (img_size, img_size))
			# cv2.imshow('a',imgs[i,:,:,:]/255.)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()
			# print('max img val: ', np.amax(imgs[0]))
			# labels[i,:] = num_attributes[int(idx[i]+1)]


		# print('labels: ', labels.shape)
		# print('imgs: ', imgs.shape)
		temp = [i for i in temp if i not in idx]
		temp = np.array(temp)



		if split == 'train':
			copy_train_idx = temp
		elif split == 'valid':
			copy_val_idx = temp
		elif split == 'test':
			copy_test_idx = temp

		#make range of values of image from -1 to 1
		imgs = imgs/127.5 - 1.
		# print('image min: ', np.amin(imgs))
		# print('image max: ', np.amax(imgs))
		# yield imgs
		# yield imgs, labels
		yield imgs, None




optimizer = optimizers.rmsprop(0.0003)
#########
'''VAE'''
#########

'''
encoder = build_encoder(img_size=img_size, channels=channels)
decoder = build_generator()


inputs = Input(shape=(img_size, img_size, channels))
z_mean, z_log_var, z = encoder(inputs)
outputs = decoder(z)
print('output shape: ', outputs.get_shape())
vae = Model(inputs,outputs, name='vae')


models = (encoder, decoder)
x_test = dataGenerator('test', batch_size=batch_size, img_size=img_size, channels=channels)
# data = (x_test, y_test)

original_dim = 64*64*3
# reconstruction_loss = binary_crossentropy(inputs, outputs)
reconstruction_loss = mse(K.flatten(inputs), K.flatten(outputs))
# print()
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
vae_loss = K.mean(reconstruction_loss + kl_loss)


#for likelihood loss, see: https://github.com/chainer/chainer/blob/master/chainer/functions/loss/vae.py
#and see: https://docs.chainer.org/en/stable/reference/generated/chainer.functions.gaussian_nll.html
# x_prec = K.exp(-z_log_var)
# x_diff = z - z_mean
# x_power = (x_diff * x_diff) * x_prec * -0.5
# loss = (z_log_var + math.log(2 * math.pi)) / 2 - x_power
# like_loss = K.sum(loss)
# print('vae_loss: ', vae_loss.get_shape())



vae.add_loss(vae_loss)
optimizer = optimizers.rmsprop(0.0003)

# vae.add_loss(like_loss)                                                                           
vae.compile(optimizer=optimizer)
vae.summary()


# vae.fit(x_train, epochs=epochs, batch_size=batch_size, validation_data=(x_test,None))
# a,b = next(dataGenerator('train'))
# print('a: ', a.shape)
if predict_only==False:
	checkpointer = ModelCheckpoint(filepath=checkpt_path + '/model-{epoch:02d}.hdf5', verbose=1)
	vae.fit_generator(dataGen(batch_size=batch_size, img_size=img_size, channels=channels), len(train_idx)//batch_size, epochs, callbacks=[checkpointer])

	vae.save_weights('vae_finalweights.hdf5')

	# data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
	# plot_results(models, data, batch_size=batch_size,model_name="vae")

	data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))

	out = vae.predict(data)
	out = (out+1.)*127.5
	data = (data+1.)*127.5
	for i in range(data.shape[0]):
		cv2.imshow('output', out[i].astype('uint8'))
		cv2.imshow('data', data[i].astype('uint8'))
		cv2.waitKey(0)
		cv2.destroyAllWindows()
	# vae.predict()


else:
	vae.load_weights('vae_finalweights.hdf5')
#	plot_results(models, (x_test,None), batch_size=batch_size,model_name="vae")
	data,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))

	out = vae.predict(data)
	out = (out+1.)*127.5
	data = (data+1.)*127.5
	for i in range(data.shape[0]):
		cv2.imshow('output', out[i].astype('uint8'))
		cv2.imshow('data', data[i].astype('uint8'))
		cv2.waitKey(0)
		cv2.destroyAllWindows()
'''







#optimizer = Adam(0.0002, 0.5)

generator = build_generator()
discriminator = build_discriminator()

discriminator.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])

n_disc_trainable = len(discriminator.trainable_weights)
print('n_disc_trainable: ', n_disc_trainable)

z = Input(shape=(z_dim,))
img = generator(z)
# print('\n\n\n\nimg: ', img.get_shape)
# print('\n\n\n\n')

n_gen_trainable = len(generator.trainable_weights)
print('n_gen_trainable: ', n_gen_trainable)

discriminator.trainable = False
# discriminator.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
validity = discriminator(img)
combined = Model(z, validity)
combined.compile(loss='binary_crossentropy', optimizer=optimizer)

print("\n\nafter compiling combined...")
discriminator.trainable = True
n_disc_trainable = len(discriminator.trainable_weights)
print('n_disc_trainable: ', n_disc_trainable)
print('len(discriminator._collected_trainable_weights): ', len(discriminator._collected_trainable_weights))
n_gen_trainable = len(generator.trainable_weights)
print('n_gen_trainable: ', n_gen_trainable)
print('len(combined._collected_trainable_weights): ', len(combined._collected_trainable_weights))
##############
'''TRAINING'''
##############

if load_old==True:
	combined.load_weights(old_wts_path)


# Adversarial ground truths
valid = np.ones((batch_size, 1))
fake = np.zeros((batch_size, 1))

for epoch in range(epochs):

    # ---------------------
    #  Train Discriminator
    # ---------------------

    # Select a random batch of images
    # idx = np.random.randint(0, X_train.shape[0], batch_size)
    # imgs = X_train[idx]
	imgs,_ = next(dataGen(batch_size=batch_size, img_size=img_size, channels=channels))
	#imgs = imgs/127.5 - 1.
	# print('imgs shape:', imgs.shape)
	# print('valid shape: ', valid.shape)

	noise = np.random.normal(0, 1, (batch_size, z_dim))
	# noise = np.random.uniform(-1., 1., (batch_size, z_dim))

    # Generate a batch of new images
	gen_imgs = generator.predict(noise)
	# print('gen_imgs.shape: ', gen_imgs.shape)

    # Train the discriminator
	d_loss_real = discriminator.train_on_batch(imgs, valid)
	d_loss_fake = discriminator.train_on_batch(gen_imgs, fake)
	d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

    # ---------------------
    #  Train Generator
    # ---------------------

	noise = np.random.normal(0, 1, (batch_size, z_dim))
	# noise = np.random.uniform(-1., 1., (batch_size, z_dim))

    # Train the generator (to have the discriminator label samples as valid)
	g_loss = combined.train_on_batch(noise, valid)

    # Plot the progress
	print ("epoch: %d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, d_loss[0], 100*d_loss[1], g_loss))

    # If at save interval => save generated image samples
	if epoch % sample_interval == 0:
		r, c = 5, 5
		noise = np.random.normal(0, 1, (r * c, z_dim))
		# noise = np.random.uniform(-1., 1., (r * c, z_dim))
		gen_imgs = generator.predict(noise)

		# Rescale images 0 - 1
		gen_imgs = 0.5 * gen_imgs + 0.5

		fig, axs = plt.subplots(r, c)
		cnt = 0
		for i in range(r):
		    for j in range(c):
		        # cv2.imshow('sample out: ', (gen_imgs[cnt, :,:,:]))
		        # cv2.waitKey(0)
		        # cv2.destroyAllWindows()
		        b, g, r = cv2.split(gen_imgs[cnt, :,:,:])
		        rgb_img = cv2.merge([r,g,b])
		        axs[i,j].imshow(rgb_img)
		        axs[i,j].axis('off')
		        cnt += 1
		fig.savefig("images/%d.png" % epoch)
		combined.save_weights('images/model%d.hdf5' % epoch)
		plt.close()