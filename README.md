This is the implementation of the VAE-GAN model as described in the paper:
"Autoencoding beyond pixels using a learned similarity metric" by Larsen et al.

This software is intended for the requirements in 
EE298 - Deep Learning Experiments handled by Prof. Rowel Atienza 
from University of the Philippines (Diliman)

Students:
Rangel Daroya
Richard Guinto

Documentation can be found at:
https://docs.google.com/presentation/d/1XdGfIyO7muMwmsT50JeOeZcGdLpiBpZMcGJ1brVtwYI/edit?usp=sharing

Please go to the "final code and models" folder for our final implementation, then
run python3 complete_vaegan.py