#!/usr/bin/env python

import sys
import glob
import os.path
import uuid

from queue import Queue, Empty
from threading import Thread

import numpy as np
import cv2


def scale_down(img, target_size):
    return cv2.resize(img, target_size, interpolation=cv2.INTER_AREA)


def crop(img, origin, size):
    width, height = size
    x, y = origin
    return img[y:y + height, x:x + width]


# opencv image shape is expressed in terms of height x width
# while target size is expressed in terms of width x height
def center_crop(img, target_size):
    src_height, src_width = img.shape
    src_ratio = src_width / src_height
    #print('src height: %d width: %d ratio: %f' % (src_height, src_width, src_ratio))
    target_width, target_height = target_size
    target_ratio = target_width / target_height
    #print('target height: %d width: %d ratio: %f' % (target_height, target_width, target_ratio))
    if src_ratio < target_ratio: # e.g. src is 4:3 vs target is 16:9, so crop the height
        cropped_height = int(np.round(src_width / target_ratio))
        cropped_width = src_width
        y = int(np.round((src_height - cropped_height)/2))
        x = 0
    else:
        cropped_width = int(np.round(src_height * target_ratio))
        cropped_height = src_height
        x = int(np.round((src_width - cropped_width)/2))
        y = 0
    #print('return x: %d y: %d width: %d height: %d' % (x,y, cropped_width, cropped_height))
    return crop(img, (x, y), (cropped_width, cropped_height))


def process_image(image_path, num_output=1):
    # Read as grayscale
    img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    target_size = (64, 64)
    img = center_crop(img, target_size)
    img = scale_down(img, target_size)

    images = []
    filenames = []
    while len(images) < num_output:
        images.append(img)
        filenames.append(os.path.basename(image_path))
    print('done:', image_path)
    return images, filenames


class Worker(Thread):

   def __init__(self, input_queue, output_queue, num_samples):
       Thread.__init__(self)
       self.input_queue = input_queue
       self.output_queue = output_queue
       self.num_samples = num_samples

   def run(self):
       while True:
           img_path = self.input_queue.get()
           if img_path is None:
               break
           output = process_image(img_path, self.num_samples)
           self.input_queue.task_done()
           if output is not None:
               self.output_queue.put(output)


def pack(outdir, images, filenames):
    name = str(uuid.uuid4())
    pack = os.path.join(outdir, name + '.npz')
    images = np.stack(images)
    filenames = np.stack(filenames)
    with open(pack, 'wb') as f:
        np.savez(f, images=images, filenames=filenames)
    print('bundled:', name)


def bundle(queue, outdir):
    images = []
    filenames = []
    
    while True:
        try:
            i,f = queue.get(timeout=10)
        except Empty:
            break
        images.extend(i)
        filenames.extend(f)

        if len(images) >= 1024*8:
            pack(outdir, images)
            images = []
            filenames = []
        queue.task_done()

    if images:
        pack(outdir, images, filenames)


def main():
    if len(sys.argv) < 4:
        print('Usage: generate.py <output dir> <INPUT DIRS...>')
        exit(1)
    output_dir = sys.argv[1]
    samples = 1
    input_dirs = sys.argv[2:]

    # Create a queue to communicate with the worker threads
    input_queue = Queue()
    output_queue = Queue()

    num_workers = 8
    workers = []
    # Create worker threads
    for i in range(num_workers):
        worker = Worker(input_queue, output_queue, samples)
        worker.start()
        workers.append(worker)

    for d in input_dirs:
        for i in glob.iglob(os.path.join(d, '*.jpg')):
            input_queue.put(i)

    bundle(output_queue, output_dir)

    input_queue.join()
    for i in range(num_workers):
        input_queue.put(None)
    for worker in workers:
        worker.join()


if __name__ == '__main__':
    main()